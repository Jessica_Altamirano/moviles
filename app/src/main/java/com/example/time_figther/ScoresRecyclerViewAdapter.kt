package com.example.time_figther

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_main_game.view.*

class ScoresRecyclerViewAdapter: RecyclerView.Adapter<ScoresViewHolder>() {

    val db= FirebaseFirestore.getInstance()
    val players: MutableList<Player> = mutableListOf<Player>()

    init{
        val playersRef= db.collection("players")

        playersRef
            .orderBy("score")
            .addSnapshotListener{snapshot, error ->
            if(error != null){
                return@addSnapshotListener
            }
            //me sirve para escuchar todos los que tengan la aplicacion, notifica a todos los elementos en tiempo real
            //Log.d("ADAPTER","INFO: ${snapshot!!.metadata}")
            for(doc in snapshot!!.documentChanges){

                when(doc.type){
                    DocumentChange.Type.ADDED ->{
                        val player = Player(
                            doc.document.getString("name")!!,
                            doc.document.getDouble("score")!!.toInt()
                        )
                        //Log.d("ADAPTER","INFO: ${snapshot!!.metadata}")
                        players.add(player)
                        notifyItemInserted(players.size - 1)
                    }
                    else -> return@addSnapshotListener
                }

            }
        }
    }
    //recuperar el viewholder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoresViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.scores_view_holder, parent, false)
        return ScoresViewHolder(view)
    }
    //ver cuantos items existen en el datahouse
    override fun getItemCount(): Int {
        return players.size
    }
    //ver a quien le corresponde
    override fun onBindViewHolder(holder: ScoresViewHolder, position: Int) {
        holder.playerName.text= players[position].name
        holder.playerScore.text = players[position].score.toString()
    }
}