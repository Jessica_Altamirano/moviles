package com.example.time_figther

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.media.session.PlaybackStateCompat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver



class MainGame : Fragment() {

    internal lateinit var welcomMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button

    @State
    var score=0

    @State
    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long =100
    private lateinit var countDownTimer: CountDownTimer // para implementar la varibale timer

    @State
    var timeLeft = 10;



    private val args: MainGameArgs by navArgs()




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        StateSaver.restoreInstanceState(this,savedInstanceState)

        gameScoreTextView= view.findViewById(R.id.score)
        goButton = view.findViewById(R.id.go_button)
        timeLeftTextView = view.findViewById(R.id.time_left)

        // guardar el estado de nuestras variables y poderlas reutilizar nuestras variables
       /* if (savedInstanceState != null){
            score=savedInstanceState.getInt(SCORE_KEY)
        }*/
        gameScoreTextView.text =getString(R.string.score_i,score)
        timeLeftTextView.text =getString(R.string.time_left_i, timeLeft)

        welcomMessage= view.findViewById(R.id.welcome_message)
        val playerName = args.playerName
        // funcion get strgin representa a mi resources y luego busco el que cree para el mensaje
        //id de mi string y reemplazo %s lo que hace to String
        welcomMessage.text = getString(
            R.string.welcome_player,playerName.toString()
        )


        goButton.setOnClickListener { incrementScore()}
        resetGame()

        if(gameStarted){
            resetGame()
            return
        }

    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this,outState)
        countDownTimer.cancel()
        // le ponemos un contador para que ya no vuelva a girar
        //outState.putInt(SCORE_KEY,score)
        // le estamos poniendo en el estado, guardandole un entero con la llave score-key y score
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }


    companion object {

        private val SCORE_KEY="SCORE"
    }

    fun incrementScore(){


        if(!gameStarted){
            countDownTimer.start()

        }

        score ++
        gameScoreTextView.text = getString(R.string.score_i, score)
    }

    private fun resetGame(){
        score= 0
        gameScoreTextView.text =getString(R.string.score_i,score)

        timeLeft = 10
        timeLeftTextView.text =getString(R.string.time_left_i,timeLeft)

        countDownTimer= object :CountDownTimer(initialCountDown,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.time_left_i, timeLeft)
            }

            override fun onFinish() {
                endGame()

            }

        }
    }

    private fun startGame(){

        countDownTimer.start()
        gameStarted = true

    }

    private fun restoreGame(){
        gameScoreTextView.text =getString(R.string.score_i,score)

        countDownTimer= object :CountDownTimer
            (timeLeft * 1000L,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.time_left_i, timeLeft)
            }

            override fun onFinish() {
                endGame()

            }

        }
        countDownTimer.start()
    }

    private fun endGame(){
        Toast.makeText(
            activity,
            getString(R.string.end_game, score),
            Toast.LENGTH_LONG).show()

        resetGame()

    }


}
