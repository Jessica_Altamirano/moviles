
open class SmartPhoneBuilder{
    private var OS:String? = null
    private var RAM:Int? = null
    private var pantalla:String? = null
    private var procesador:Double? = null
    private var expansion:String? = null

    fun setOS(OS:String){
        this.OS = OS
    }
    fun getOS():String?{
        return this.OS
    }
    fun setRAM(RAM:Int){
        this.RAM = RAM
    }
    fun getRAM():Int?{
        return this.RAM
    }
    fun setPantalla(pantalla:String){
        this.pantalla = pantalla
    }
    fun getPantalla():String?{
        return this.pantalla
    }
    fun setProcesador(Procesador:Double){
        this.procesador = procesador
    }
    fun getProcesador():Double?{
        return this.procesador
    }
    fun setExpansion(expansion:String){
        this.expansion = expansion
    }
    fun getExpansion():String?{
        return this.expansion
    }
}

class SmartPhone{

    var sp:SmartPhoneBuilder
    constructor(smartBuild:SmartPhoneBuilder) {
        this.sp = smartBuild
    }

    fun build():String{

        return ("Las caracteristicas tecnicas del smartphone es :" +
                "\nSistema Operativo : ${sp.getOS()} " +
                "\nRAM : ${sp.getRAM()} " +
                "\nPantalla : ${sp.getPantalla()} " +
                "\nProcesador : ${sp.getProcesador()} " +
                "\nExpansion : ${sp.getExpansion()}"
                )
    }
}

fun main(args: Array<String>) {
    var smartBuilder = SmartPhoneBuilder()
    smartBuilder.setOS("Android 9.0")
    smartBuilder.setRAM(4)
    smartBuilder.setPantalla("1080 x 2340 pixels")
    smartBuilder.setExpansion("miscroSD")

    var smartP = SmartPhone(smartBuilder)
    println(smartP.build())
}