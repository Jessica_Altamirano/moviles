package main

interface Autos {
    val color : String
    val marca : String

    fun precio()
    fun anio()


}

class Todoterreno(override val color: String, override val marca: String): Autos {
    override fun precio() {
        println("Todoterreno \n" +
                "precio: 50000 , color: $color, marca: $marca ")
    }
    override fun anio(){
        println("año: 2000")
    }

}

class Familiar(override val color: String, override val marca: String): Autos {
    override fun precio() {
        println("Familiar \n" +
                "precio:25000, color : $color, marca: $marca")
    }
    override fun anio(){
        println("año: 2010")
    }
}

class Ejecutivo (override val color: String, override val marca: String): Autos {
    override fun precio() {
        println("Ejecutivo \nprecio: 55000, color: $color, marca: $marca")
    }
    override fun anio(){
        println("año: 2015")
    }
}

class AutosFactory  {
    fun getAutos(autosType: String): Autos {
        return when(autosType.trim()) {
            "Auto Familiar" -> Familiar("blanco","chevrolet")
            "Auto Ejecutivo" -> Ejecutivo("verde","Audi")
            "Auto Todoterreno" -> Todoterreno("rojo","BMW")
            else -> throw RuntimeException("Unknown shape $autosType")
        }
    }

}

fun main(args: Array<String>) {
    val autosFactory = AutosFactory()

    val auto1: Autos = autosFactory.getAutos("Auto Familiar")
    auto1.precio()
    auto1.anio()

    val auto2: Autos = autosFactory.getAutos("Auto Ejecutivo")
    auto2.precio()
    auto2.anio()

    val auto3: Autos = autosFactory.getAutos("Auto Todoterreno")
    auto3.precio()
    auto3.anio()
}